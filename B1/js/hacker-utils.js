// JavaScript Document
// Created By Camilo Medina // dev.cmedina@gmail.com
// 2015 - 2016, update september 2018 version 2.018

$.urlParam = function (name)
{
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null)
    {
        return null;
    }
    else
    {
        return decodeURI(results[1]) || 0;
    }
}

jQuery.extend({
    highlight: function (node, re, nodeName, className, id)
    {
        if (node.nodeType === 3)
        {
            var match = node.data.match(re);
            if (match)
            {
                var highlight = document.createElement(nodeName || 'span');
                highlight.className = className || 'highlight';
                highlight.setAttribute("data-means-id", id);
                var wordNode = node.splitText(match.index);
                wordNode.splitText(match[0].length);
                var wordClone = wordNode.cloneNode(true);
                highlight.appendChild(wordClone);
                wordNode.parentNode.replaceChild(highlight, wordNode);
                return 1; //skip added node in parent
            }
        } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
            !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
            !(node.tagName === nodeName.toUpperCase() && node.className === className))
        { // skip if already highlighted
            for (var i = 0; i < node.childNodes.length; i++)
            {
                i += jQuery.highlight(node.childNodes[i], re, nodeName, className, id);
            }
        }
        return 0;
    }
});

jQuery.fn.unhighlight = function (options)
{
    var settings = { className: 'highlight', element: 'span' };
    jQuery.extend(settings, options);

    return this.find(settings.element + "." + settings.className).each(function ()
    {
        var parent = this.parentNode;
        parent.replaceChild(this.firstChild, this);
        parent.normalize();
    }).end();
};

jQuery.fn.highlight = function (words, options)
{
    var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false, id: -0 };
    jQuery.extend(settings, options);

    if (words.constructor === String)
    {
        words = [words];
    }
    words = jQuery.grep(words, function (word, i)
    {
        return word != '';
    });
    words = jQuery.map(words, function (word, i)
    {
        return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    });
    if (words.length == 0) { return this; };

    var flag = settings.caseSensitive ? "" : "i";
    var pattern = "(" + words.join("|") + ")";
    if (settings.wordsOnly)
    {
        pattern = "\\b" + pattern + "\\b";
    }
    var re = new RegExp(pattern, flag);

    return this.each(function ()
    {
        jQuery.highlight(this, re, settings.element, settings.className, settings.id);
    });
};

function parseGlossary()
{
    console.log("Parsing Glossary");
    for (var i = 0; i < glossaryWords.length; i++)
    {
        $('body').highlight(glossaryWords[i].word, { element: 'span', className: 'glossary-word', id: i });
    }
}

function acumOffset(element)
{
    var top = 0, left = 0;
    do
    {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);

    return {
        top: top,
        left: left
    };
};

if (glossaryWords == 'undefined')
{
    var glossaryWords = [
        {
            'word': 'tautología',
            'means': 'Que puede fallar o equivocarse. En términos científicos, se puede demostrar que una teoría es errónea mediante posteriores investigaciones'
        }
    ];
}

function initChoiceList()
{
    $('[data-chooser]').addClass('hidden-mobile').hide();
    var currentOpener;
    initPicker();

    $(document).on('click', '.line', function (e)
    {
        master = $(this).parents('[data-target]');
        var ref = $(this);
        currentOpener = $(this);

        $('[data-chooser="' + master.data('target') + '"]').css("left", ref.offset().left - 10);
        $('[data-chooser="' + master.data('target') + '"]').css("top", ref.offset().top);
        $('[data-chooser="' + master.data('target') + '"]').slideDown(100);
    });

    $('[data-chooser]').on('click', 'li', function (e)
    {
        $(this).is('[data-option]') ? currentOpener.text('(' + $(this).data('option') + ')') : currentOpener.text('' + $(this).text() + '');
        $('[data-chooser]').slideUp(70);
    });

    $(document).on('click', '.btnPicker', function (e)
    {
        master = $(this).parents('[data-target]');
        var ref = $(this);
        currentOpener = $(this);

        $('[data-chooser="' + master.data('target') + '"]').css("left", ref.offset().left - 10);
        $('[data-chooser="' + master.data('target') + '"]').css("top", ref.offset().top);
        $('[data-chooser="' + master.data('target') + '"]').slideDown(100);
    });

    function initPicker()
    {
        var configPicker = {
            init: function ()
            {
                this.createBtnPikcer();
                this.viewList();
                this.configChooser();
            },

            createBtnPikcer: function ()
            {
                $('[data-action]').each(function ()
                {
                    var btnPicker = document.createElement('b');
                    btnPicker.className = 'btnPicker';
                    btnPicker.style.cursor = 'pointer';
                    btnPicker.innerHTML = '(__)';

                    chooserUI = $(this).data();
                    if (chooserUI.position === 'start')
                    {
                        $(this).children('li').prepend(btnPicker);
                    } else
                    {
                        btnPicker.style.marginLeft = '5px';
                        btnPicker.style.cssFloat = 'initial';
                        $(this).children('li').append(btnPicker);
                    }
                });
            },
            viewList: function ()
            {
                $('[data-action]').each(function ()
                {
                    chooserUI = $(this).data();
                    if (chooserUI.show === true)
                    {
                        var viewBox = $('<ul></ul>');
                        contentList = $('[data-chooser="' + chooserUI.target + '"]').html();
                        viewBox.html('').append(contentList);
                        $(this).before(viewBox);
                    }
                });
            },
            configChooser: function ()
            {
                $('[data-action]').each(function ()
                {
                    chooserUI = $(this).data();

                    $('[data-chooser="' + chooserUI.target + '"] li').each(function ()
                    {
                        text = $(this).children('b').text();
                        $(this).attr({ 'data-option': text.replace(/[\. ,:-]+/g, '') });
                        $(this).children('b').hide();
                    });
                });
            },
        }

        configPicker.init();
    }
}

function initRemeber()
{
    var settings = {
        overlay: $('<div id="hk-popup" class="popup-container"></div>'),
        container: $('<div class="container-tip"></div>'),
        header: $('<div class="header">Interés <span class="close-tip glyphicon glyphicon-remove" aria-hidden="true"></span></div>'),
        body: $('<div class="body"></div>'),
        indicator: $('<span class="indicator"></span>')
    };

    $('.tip').each(function ()
    {
        this.style.color = '#f6911e';
        this.style.fontWeight = 'bold';
        this.style.cursor = 'pointer';
    });

    $(document).on('click', '.tip', function (e)
    {
        var key = $(this).text();
        var word = $(this).data('word');
        var element = $(this);

        let i = rememberWord.findIndex((data) => data.key === word);
        content = rememberWord[i].content;

        renderTip(content, element, word);
    });

    $(document).on('click', '.close-tip', function (e)
    {
        settings.container.animate({ top: '100%' }, 150);
        settings.overlay.animate({ opacity: 0 }, 150, function ()
        {
            settings.overlay.empty();
            settings.overlay.remove();
        });
    });

    function renderTip(content, element, word)
    {
        settings.body.html('').append('<p>' + content + '</p>');
        settings.container.append(settings.header.html('').append(word + '<span class="close-tip glyphicon glyphicon-remove" aria-hidden="true"></span>'));
        settings.container.append(settings.indicator);
        settings.container.append(settings.body);
        settings.overlay.append(settings.container);

        settings.container.css({ 'top': '100%' });
        settings.overlay.css({ 'opacity': 0, top: '0' });

        $('body').append(settings.overlay);
        settings.overlay.animate({ opacity: 1 }, 120);

        var absTop = element.offset().top - $(window).scrollTop();
        var absBottom = absTop + settings.container.height();
        var absLeft = element.offset().left - (settings.container.offset().left);
        settings.indicator.css("left", absLeft + (element.width() / 2));

        if ((absBottom + 60) > window.innerHeight)
        {
            settings.indicator.css({
                position: 'absolute',
                bottom: '-10px',
                top: '',
                width: '0',
                height: '0',
                'border-width': '10px 14px 0 14px',
                'border-style': 'solid',
                'border-color': '#ffffff transparent transparent transparent'
            });

            absTop -= (settings.container.height() + 30);
            settings.container.animate({ top: (absTop + 22) }, 400);
        } else
        {
            settings.indicator.css({
                position: 'absolute',
                top: '-10px',
                bottom: '',
                width: '0',
                height: '0',
                'border-width': '2px 14px 10px 14px',
                'border-style': 'solid',
                'border-color': 'transparent transparent #6fbe44 transparent'
            });
            absTop += 18;
            settings.container.animate({ top: (absTop + 8) }, 400);
        }

        settings.overlay.animate({ opacity: 1 }, 120);
    }
}
$(document).ready(function (e)
{
    initRemeber();
    initChoiceList();
    //$('table').stacktable();

    //Multiple Choice Functionality. (Just Highlight Element).
    $('ul[role="multiple-choice"]').each(function (index, element)
    {
        $(element).children("li").children("b").css("color", "#f16b78");
        $(element).children("li").addClass("is-clickable");
        $(element).children("li").click(function (e)
        {
            $(element).children("li.is-selected").removeClass("is-selected").css({ 'color': '', 'font-weight': '400' });
            $(this).addClass("is-selected").css({ 'color': '#1cb1f5', 'font-weight': '400' });
        });
    });
    $("li[data-pick-list]").each(function (t, e)
    {
        console.log("que onda");
        var a = $(e).data("pick-list").split("|"),
            n = "";
        n = 2 < a.length ? 'class="col-' + a.length + '"' : 'class="triple-column"';
        for (var i = $("<ul " + n + "></ul>"), l = $(e).children('span[role="answer"]'), r = 0; r < a.length; r++)
        {
            var s = $('<li style="margin-top:0px; padding-top:0px; "><b style="color:#F154A7!important">' + "abcdefghijklmnñopqrstuvwxyz".charAt(r) + ".</b><span>" + a[r] + "</span></li>");
            s.click(function (t)
            {
                i.children("li").css("font-weight", "normal"), $(this).css("font-weight", "bold"), l.html($(this).children("span").text()), l.css({
                    "text-decoration": "underline",
                    color: "#0333FF"
                })
            }), i.append(s);
        }
        $(e).append(i);
    });
    $('ul[role="single-choice"]').each(function (index, element)
    {
        $(element).children("li").addClass("is-clickable");
        $(element).children("li").click(function (e)
        {
            if ($(element).data("state") == "done")
                return;

            $(element).data("state", "done");
            $(element).children("li.is-selected").removeClass("is-selected");
            if ($(this).attr("role") == "success")
                $(this).addClass("success");
            else
                $(this).addClass("fail");
            $(element).children("li").removeClass("is-clickable");
        });
    });

    $('ul[role="multiple-select"]').each(function (index, element)
    {
        $(element).children("li").children("b").css("color", "#EB08AC");
        $(element).children("li").addClass("is-clickable");
        $(element).children("li").click(function (e)
        {
            if ($(this).is('.is-selected'))
            {
                $(this).removeClass('is-selected').css('color', '');
            }
            else
            {
                $(this).addClass('is-selected').css('color', '#05ADE8');
            }
        });
    });

    $('ul[role="multiple-choice-mark"]').each(function (index, element)
    {
        $(element).children("li").addClass("is-clickable");
        $(element).children("li").click(function (e)
        {
            $(element).children('li.is-selected').children('.btnChoise').css('background', '#cfd8dc');
            $(element).children('li.is-selected').removeClass('is-selected').css('color', '');
            $(this).addClass("is-selected").css("color", '#05ADE8');
            $(this).children('b').css('background', '#05ADE8');
        });
    });

    $('[role="multiple-choice-mark"]').each(function ()
    {
        var btnChoice = document.createElement('b');
        btnChoice.className = 'btnChoise';
        btnChoice.style.cursor = 'pointer';
        btnChoice.style.background = '#cfd8dc';
        btnChoice.style.width = '20px';
        btnChoice.style.height = '20px';
        btnChoice.style.marginLeft = '-24px';
        btnChoice.style.borderRadius = '3px';
        // btnChoice.innerHTML ='&nbsp;';
        chooserUI = $(this).data();

        if (chooserUI.position !== 'end')
        {
            $(this).children('li').prepend(btnChoice);
        } else
        {
            btnChoice.style.marginLeft = '5px';
            btnChoice.style.cssFloat = 'initial';
            btnChoice.style.display = 'inline-block';
            btnChoice.style.verticalAlign = 'middle';
            $(this).children('li').append(btnChoice);
        }
    });

    $('div[data-button-options]').each(function (index, element)
    {
        var buttons = $(element).data("button-options").split("|");
        for (var x = 0; x < buttons.length; x++)
        {
            $(element).append('<button type="button" >' + buttons[x] + '</button>');
        }
        $(element).data("temp-val", 0);
    });

    $('div[data-toggle="btn-radio"]:not([data-button-options])').append('<button type="button" >Sí</button><button type="button">No</button>');
    $('div[data-toggle="btn-radio"]').children("button").click(function (e)
    {
        $(this).parent().children("button").removeClass("checked");
        $(this).addClass("checked");

        var nP = $(this).parent();
        if (nP.has('[data-button-values]'))
        {
            var values = nP.data('button-values').split("|");
            nP.data("temp-val", values[nP.children("button").index(this)]);
            var total = 0.0;
            $('[data-group="' + nP.data("group") + '"]').each(function (index, element)
            {
                total += parseFloat($(element).data("temp-val"));
                $(nP.data('button-target')).text(total);
            });
        }
    });

    $('td').has('div[data-toggle]').addClass('vcenter');
    $('div.note.has-border').append('<span class="note-border"></span>');
    $('img.clickable').click(function (e)
    {
        e.preventDefault();
        document.location.href = "image://" + $(this).attr("src");
    })

    /*LEVEL INDICATOR*/
    $('div[role="level-indicator"]').each(function (index, element)
    {
        $(element).append('<div class="holder">' + $(element).data('label') + '</div>');

        var step = 1;
        if ($(element).data('step'))
            step = $(element).data('step');

        var minValue = 1;
        if ($(element).data('min'))
            minValue = $(element).data('min');

        var maxValue = $(element).data('max');
        var current = $(element).data('current');
        var totalSteps = (maxValue / step) - (minValue / step)
        var increment = 60 / totalSteps

        var act_Index = (current / step) - (minValue / step);

        var inc_R = 38 / totalSteps;
        var inc_G = 159 / totalSteps;

        var final_R = parseInt(217 + (inc_R * act_Index));
        var final_G = parseInt(221 - (inc_G * act_Index));

        if (final_R > 255)
            final_R = 255;

        for (i = 0; i <= totalSteps; i++)
        {
            var temp = $('<span style="height:' + (40 + (increment * i)) + '%;">' + (minValue + (step * i)) + '</span>');
            if ((minValue + (step * i)) <= current)
            {
                console.log("COLOR", 'rgba(' + final_R + ',' + final_R + ',0,1.00)');
                temp.css({
                    'background-color': '#5dbf7b',
                    'color': '#fff'
                });
            }
            $(element).append(temp);
        }
    });

    $('.katex').each(function ()
    {
        var mathText = $(this).html();
        try
        {
            $(this).html(katex.renderToString(mathText));
        }
        catch (err)
        {
            alert(err.message);
        }
    });

    $('[data-storable]').each(function (index, element)
    {
        var e = $(element);
        if ($.urlParam("desktop") != null)
        {
            e.append('<div class="button-container"><a class="button button-default" href="http://createNote/' + e.prop("id") + '/' + e.data('storable') + '"> Guardar actividad </a></div>');
        } else
        {
            e.append('<div class="button-container"><a class="button button-default" href="note://' + e.prop("id") + '/' + e.data('storable') + '"> Guardar actividad </a></div>');
        }
    });

    $('[role="en"]').each(function (index, element)
    {
        $(element).find('a.button-default').html('').append('Save activity');
    });

    //GLOSSARY SECTION
    $('figure').each(function (index, element)
    {
        var img = $(element).children("img");
        var wi = img.width();
        img.addClass("img-responsive");
        if (wi > 500)
            wi -= 50;
        //img.css("max-width",wi);
        $(element).css("max-width", wi);
        $(element).children("figcaption").css("max-width", wi);
    });

    $('ul[role="dual-picker"]').each(function (index, element)
    {
        var container = $('<div style="display: inline-block;"></div>');
        var config = $(element).data("values").split('|');
        var separator = "";
        for (var i = 0; i < config.length; i++)
        {
            var btn = $('<a>&nbsp;' + config[i] + '&nbsp;</a>');
            btn.css("cursor", "pointer");
            btn.css("color", "#999999");
            btn.click(function (e)
            {
                e.preventDefault();
                $(this).css("color", "");
                $(this).nextAll().remove();
                $(this).prevAll().remove();
            })
            container.append(separator);
            container.append(btn);
            separator = "<span>&nbsp;/&nbsp;</span>";
        }
        container.prepend("(").append(")");
        container.css("color", "#20b2aa");
        $(element).children("li").append(container);
    });

    //THE FUCKING DATE PICKER ANTI "Rockie"
    $('ul[role="data-picker"]').each(function (index, element)
    {
        var dataSource = $(element).data("source"); var mFlag = $(element).data("keep-alive"); var spanBtn = $('<span style="color:rgba(0,168,221,1.00); margin-left:5px; margin-right:5px; display:inline-block; cursor:pointer;">(__)</span>'); spanBtn.click(function (e)
        {
            var pContainer = $('<div id="hk-popup" class="popup-container" style="display:block; top:0px;"></div>'); var floatingWindow = $('<div style="position:absolute; background:#B8E3F1; border:solid 2px #FFF; box-shadow:2px 2px 2px #A0A0A0; display:block; opacity:0; " class="rounded-corner"></div>')
            var rootList = $('<ul style="padding:10px; margin:0px;"></ul>'); var arrow = $('<span></span>'); var currentBtn = $(this); $('#' + dataSource).children("li").each(function (index, element)
            {
                var bRef = $(element); if (bRef.data("disabled") == undefined)
                {
                    var bItem = bRef.children("b").text(); var text = bRef.text().replace(bItem, ''); var mItem = $('<li style="padding:0px; margin:0px; font-weight:bold; padding:2px 0px; color:#333;">' + $.trim(text) + '</li>'); mItem.click(function (e)
                    {
                        currentBtn.html("(&nbsp;" + bItem + "&nbsp;)"); if (!mFlag) { bRef.css({ "color": "red", "text-decoration": "line-through" }); bRef.data("disabled", 1) }
                        pContainer.fadeOut("fast", function (e) { pContainer.remove(); rootList.empty(); currentBtn.unbind("click") })
                    }); rootList.append(mItem)
                }
            }); if (rootList.children().length == 0)
                return; pContainer.click(function (e) { pContainer.fadeOut("fast", function (e) { pContainer.unbind("click"); pContainer.remove(); rootList.empty() }) }); floatingWindow.append(rootList); floatingWindow.append(arrow); pContainer.append(floatingWindow); $('body').append(pContainer); var absTop = currentBtn.offset().top - $(window).scrollTop(); var absBottom = absTop + floatingWindow.height(); if ((absBottom + 60) > window.innerHeight) { arrow.addClass('arrow-down'); absTop -= (floatingWindow.height()) } else { arrow.addClass('arrow-up'); absTop += 18 }
            var absLeft = currentBtn.offset().left; if (absLeft + floatingWindow.width() > window.innerWidth - 22) { arrow.css("left", floatingWindow.width() - 22); absLeft -= floatingWindow.width() - 23 } else { arrow.css("left", 22); absLeft -= 20 }
            pContainer.fadeIn("fast"); floatingWindow.css("top", absTop); floatingWindow.css("left", absLeft); floatingWindow.animate({ opacity: 1 }, 400)
        });/*$(element).children("li").addClass("is-clickable");*/if ($(element).data("position") == "start")
            $(element).children("li").prepend(spanBtn); else $(element).children("li").append(spanBtn)
    })

    try
    {
        if (glossaryWords != 'undefined')
        {
            parseGlossary();
            $('.glossary-word').click(function (e)
            {
                var target = $(this);

                if ($('#hk-popup').length == 0)
                {
                    var pContainer = $('<div id="hk-popup" class="popup-container"></div>');
                    var pWindow = $('<div class="popup-window"></div>');
                    var pBody = $('<p class="popup-body" id="sec-body-tag"></p>');
                    var pCopy = $('<a class="popup-copy">Cerrar</a>')

                    pCopy.click(function (e)
                    {
                        pContainer.animate(
                            {
                                top: "100%", opacity: 0
                            }, 400, function ()
                            {
                                pContainer.remove();
                            });
                    });

                    pContainer.append(pWindow);
                    pWindow.append(pBody);
                    pWindow.append(pCopy);

                    pBody.text(glossaryWords[$(this).data("means-id")].means);
                    pWindow.css("opacity", 0);

                    $('body').append(pContainer);

                    //COMPUTE THE RIGHT OFFSET TO BE PLACED
                    var absTop = target.offset().top - $(window).scrollTop();
                    var absBottom = absTop + pWindow.height();
                    var arrow = $('<span></span>');
                    pWindow.append(arrow);

                    if ((absBottom + 60) > window.innerHeight)
                    {
                        arrow.addClass('arrow-down');
                        absTop -= (pWindow.height() + 30);
                    } else
                    {
                        arrow.addClass('arrow-up');
                        absTop += 18;
                    }

                    var absLeft = target.offset().left - (pWindow.offset().left);
                    arrow.css("left", absLeft + (target.width() / 2));
                    pContainer.animate({ display: "block", top: "0px" }, 400);
                    //pWindow.animate({ opacity:1, top: (window.innerHeight - pWindow.height())/2 },400 );
                    pWindow.animate({ opacity: 1, top: absTop }, 400);
                }
            });
        }
    } catch (e) { }
});